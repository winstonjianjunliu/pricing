package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.security.auth.Subject;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	//observers
	public List<Observer> observers = new ArrayList<Observer>();
	
	//subscribe
	public void subscribe(Observer observer) {
		observers.add(observer);
	}
	
	//unsubscribe
	public void unsubscribe(Observer observer) {
		observers.remove(observer);
	}
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	//notify observer
	public void getPriceData() {
		try {
			String requestURL = "http://will1.conygre.com:8081/prices/" + "MRK" + "?periods=" + 4;
			BufferedReader in = new BufferedReader(new InputStreamReader
					(new URL(requestURL).openStream()));
			
			System.out.println(in.readLine());
			
			List<PricePoint> prices = new ArrayList<PricePoint>();
			
			for (int i=0; i<4; i++) {
				//in.readLine(); // header ... right? No way that could break ...
				
				String line = in.readLine();
				PricePoint price = Pricing.parsePricePoint(line);
				price.setStock("MRK");
				prices.add(price);
				//System.out.println(price);
			}
			
			//notify
			for (Observer observer: observers){
				observer.update();
			}
			
		} catch (IOException ex) {
			throw new RuntimeException
					("Couldn't retrieve price for " + "MRK" + ".", ex);
		}
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		//List<PricePoint> MRKPrice = pricing.getPriceData("MRK", 8);
		//System.out.println(MRKPrice);
		Subscriber a = new Subscriber(pricing);
		pricing.subscribe(a);
	    Subscriber b = new Subscriber(pricing);
	    pricing.subscribe(b);

	    System.out.println("First state change: 15");	
	    pricing.getPriceData();
	    System.out.println("Lovelive!");
	    pricing.getPriceData();
		
	}
}
