package com.citi.trading.pricing;

public abstract class Observer {
	public Pricing pricing;
	public abstract void update();
}
